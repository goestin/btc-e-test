import btce
import pprint
import os
import time
import prettytable
import ConfigParser

configfile='config.cfg'

# open configfile als object: config
config = ConfigParser.ConfigParser()
config.readfp(open(configfile))

# trek de gegevens uit de config file
api_key = config.get("general","key")
api_secret = config.get("general","secret")

fee=0.2

lastlast=0

pair="ltc_usd"

exchange=btce.api(api_key, api_secret)

pp = pprint.PrettyPrinter()

#x=exchange.ActiveOrders(pair)

def funds():
	result = {}
	x=exchange.GetInfo()
	for item in x['return']['funds']:
		data=float(x['return']['funds'][item])
		if item == "usd" or item == "nmc" or item == "ltc" or item == "btc":
			result[item]=data
	return(result)

def ticker(pair=str()):
	result = {}
	x=exchange.GetParam(pair,"ticker")
	for item in x['ticker']:
		data=float(x['ticker'][item])
		result[item]=data
	result['pair']=pair
	return(result)

def funds_in_dollar():
	result={}
	result['inusd']={}
	result['ticker']={}

	current_funds=funds()
	result['current_funds']=current_funds

	ltcusdticker = ticker(pair="ltc_usd")
	result['ticker']['ltc']=ltcusdticker
				
	btcusdticker = ticker(pair="btc_usd")
	result['ticker']['btc']=btcusdticker

	nmcusdticker = ticker(pair="nmc_usd")
	result['ticker']['nmc']=nmcusdticker

	for currency in current_funds:
		if currency == "usd": result['inusd'][currency] = current_funds[currency]
	
		if currency == "ltc": result['inusd'][currency] = ltcusdticker['sell'] * current_funds[currency]
	
		if currency == "btc": result['inusd'][currency] = btcusdticker['sell'] * current_funds[currency] 
		
		if currency == "nmc": result['inusd'][currency] = nmcusdticker['sell'] * current_funds[currency] 
	return(result)


table=prettytable.PrettyTable(['Currency','Amount','Selling for','Value','fee','Netto Selling Value'])
fiddata=funds_in_dollar()

x=0
for currency in fiddata['current_funds']:
	if currency != "usd":

		b = fiddata['inusd'][currency]

		table.add_row([
			currency,
			fiddata['current_funds'][currency],
			fiddata['ticker'][currency]['sell'],
			fiddata['inusd'][currency],
			( b / 100) * fee,
			b - ( b / 100) * fee
			])
		x=x + fiddata['inusd'][currency]

print(table)
print("Investment value: %.2f" % x)
print("Dollar in account: %.2f" % fiddata['current_funds']['usd'])
